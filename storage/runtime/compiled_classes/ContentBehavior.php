<?php // vTTVqPvML7oZV
/**
 * @link http://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license http://craftcms.com/license
 */

namespace craft\behaviors;

use yii\base\Behavior;

/**
 * Content behavior
 *
 * This class provides attributes for all the unique custom field handles.
 */
class ContentBehavior extends Behavior
{
    // Static
    // =========================================================================

    /**
     * @var string[] List of supported field handles.
     */
    public static $fieldHandles = [
        'consentError' => true,
        'consentForData' => true,
        'consentText' => true,
        'contactEmail' => true,
        'emailError' => true,
        'emailText' => true,
        'footerText' => true,
        'formDescription' => true,
        'nameOfPractice' => true,
        'nameOfPracticeError' => true,
        'nameOfPracticeText' => true,
        'newsletterConsent' => true,
        'requireFieldsText' => true,
        'seoDescription' => true,
        'seoImage' => true,
        'signaturesText' => true,
        'siteDescription' => true,
        'siteTitle' => true,
        'thankYouText' => true,
        'updateText' => true,
        'website' => true,
        'websiteError' => true,
        'websiteText' => true,
    ];

    // Properties
    // =========================================================================

    /**
     * @var mixed Value for field with the handle “consentError”.
     */
    public $consentError;

    /**
     * @var mixed Value for field with the handle “consentForData”.
     */
    public $consentForData;

    /**
     * @var mixed Value for field with the handle “consentText”.
     */
    public $consentText;

    /**
     * @var mixed Value for field with the handle “contactEmail”.
     */
    public $contactEmail;

    /**
     * @var mixed Value for field with the handle “emailError”.
     */
    public $emailError;

    /**
     * @var mixed Value for field with the handle “emailText”.
     */
    public $emailText;

    /**
     * @var mixed Value for field with the handle “footerText”.
     */
    public $footerText;

    /**
     * @var mixed Value for field with the handle “formDescription”.
     */
    public $formDescription;

    /**
     * @var mixed Value for field with the handle “nameOfPractice”.
     */
    public $nameOfPractice;

    /**
     * @var mixed Value for field with the handle “nameOfPracticeError”.
     */
    public $nameOfPracticeError;

    /**
     * @var mixed Value for field with the handle “nameOfPracticeText”.
     */
    public $nameOfPracticeText;

    /**
     * @var mixed Value for field with the handle “newsletterConsent”.
     */
    public $newsletterConsent;

    /**
     * @var mixed Value for field with the handle “requireFieldsText”.
     */
    public $requireFieldsText;

    /**
     * @var mixed Value for field with the handle “seoDescription”.
     */
    public $seoDescription;

    /**
     * @var mixed Value for field with the handle “seoImage”.
     */
    public $seoImage;

    /**
     * @var mixed Value for field with the handle “signaturesText”.
     */
    public $signaturesText;

    /**
     * @var mixed Value for field with the handle “siteDescription”.
     */
    public $siteDescription;

    /**
     * @var mixed Value for field with the handle “siteTitle”.
     */
    public $siteTitle;

    /**
     * @var mixed Value for field with the handle “thankYouText”.
     */
    public $thankYouText;

    /**
     * @var mixed Value for field with the handle “updateText”.
     */
    public $updateText;

    /**
     * @var mixed Value for field with the handle “website”.
     */
    public $website;

    /**
     * @var mixed Value for field with the handle “websiteError”.
     */
    public $websiteError;

    /**
     * @var mixed Value for field with the handle “websiteText”.
     */
    public $websiteText;

    /**
     * @var array Additional custom field values we don’t know about yet.
     */
    private $_customFieldValues = [];

    // Magic Property Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __isset($name)
    {
        if (isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::__isset($name);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (isset(self::$fieldHandles[$name])) {
            return $this->_customFieldValues[$name] ?? null;
        }
        return parent::__get($name);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (isset(self::$fieldHandles[$name])) {
            $this->_customFieldValues[$name] = $value;
            return;
        }
        parent::__set($name, $value);
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        if ($checkVars && isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        if ($checkVars && isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::canSetProperty($name, $checkVars);
    }
}
