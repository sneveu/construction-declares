<?php // vTTVqPvML7oZV
/**
 * @link http://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license http://craftcms.com/license
 */

namespace craft\behaviors;

/**
 * Element Query behavior
 *
 * This class provides attributes for all the unique custom field handles.
 *
 * @method self consentError(mixed $value) Sets the [[consentError]] property
 * @method self consentForData(mixed $value) Sets the [[consentForData]] property
 * @method self consentText(mixed $value) Sets the [[consentText]] property
 * @method self contactEmail(mixed $value) Sets the [[contactEmail]] property
 * @method self emailError(mixed $value) Sets the [[emailError]] property
 * @method self emailText(mixed $value) Sets the [[emailText]] property
 * @method self footerText(mixed $value) Sets the [[footerText]] property
 * @method self formDescription(mixed $value) Sets the [[formDescription]] property
 * @method self nameOfPractice(mixed $value) Sets the [[nameOfPractice]] property
 * @method self nameOfPracticeError(mixed $value) Sets the [[nameOfPracticeError]] property
 * @method self nameOfPracticeText(mixed $value) Sets the [[nameOfPracticeText]] property
 * @method self newsletterConsent(mixed $value) Sets the [[newsletterConsent]] property
 * @method self requireFieldsText(mixed $value) Sets the [[requireFieldsText]] property
 * @method self seoDescription(mixed $value) Sets the [[seoDescription]] property
 * @method self seoImage(mixed $value) Sets the [[seoImage]] property
 * @method self signaturesText(mixed $value) Sets the [[signaturesText]] property
 * @method self siteDescription(mixed $value) Sets the [[siteDescription]] property
 * @method self siteTitle(mixed $value) Sets the [[siteTitle]] property
 * @method self thankYouText(mixed $value) Sets the [[thankYouText]] property
 * @method self updateText(mixed $value) Sets the [[updateText]] property
 * @method self website(mixed $value) Sets the [[website]] property
 * @method self websiteError(mixed $value) Sets the [[websiteError]] property
 * @method self websiteText(mixed $value) Sets the [[websiteText]] property
 */
class ElementQueryBehavior extends ContentBehavior
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __call($name, $params)
    {
        if (isset(self::$fieldHandles[$name]) && count($params) === 1) {
            $this->$name = $params[0];
            return $this->owner;
        }
        return parent::__call($name, $params);
    }

    /**
     * @inheritdoc
     */
    public function hasMethod($name)
    {
        if (isset(self::$fieldHandles[$name])) {
            return true;
        }
        return parent::hasMethod($name);
    }
}
