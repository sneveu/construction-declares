<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index */
class __TwigTemplate_ea1679a4b05a0a19d1f8ae6cf6d3dec2781aa99d7d2163ddfe3024671f3ebeef extends \craft\web\twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en-US\">
<head>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
    <meta charset=\"utf-8\" />
    <title>";
        // line 6
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "siteTitle", []), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoDescription", []), "html", null, true);
        echo "\"/>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover\" />
    <meta name=\"referrer\" content=\"origin-when-cross-origin\" />

    ";
        // line 11
        if (craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoImage", []), "first", [], "method")) {
            // line 12
            echo "    <meta property=\"og:image\"             content=\"";
            echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoImage", []), "first", [], "method"), "getURL", [], "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 14
        echo "
    <meta property=\"og:site_name\"        content=\"";
        // line 15
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "siteTitle", []), "html", null, true);
        echo "\">
    <meta property=\"og:title\"            content=\"";
        // line 16
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "siteTitle", []), "html", null, true);
        echo "\">
    <meta property=\"og:description\"      content=\"";
        // line 17
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoDescription", []), "html", null, true);
        echo "\">
    <meta property=\"og:type\"             content=\"declaration\">
    <meta property=\"og:url\"              content=\"";
        // line 19
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["craft"] ?? null), "app", []), "request", []), "absoluteUrl", []), "html", null, true);
        echo "\">

    <meta name=\"twitter:card\" content=\"summary_large_image\">
    <meta name=\"twitter:url\" content=\"";
        // line 22
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["craft"] ?? null), "app", []), "request", []), "absoluteUrl", []), "html", null, true);
        echo "\">
    <meta name=\"twitter:title\" content=\"";
        // line 23
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "siteTitle", []), "html", null, true);
        echo "\">
    <meta name=\"twitter:description\" content=\"";
        // line 24
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoDescription", []), "html", null, true);
        echo "\">

    ";
        // line 26
        if (craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoImage", []), "first", [], "method")) {
            // line 27
            echo "      <meta name=\"twitter:image\" content=\"";
            echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["seo"] ?? null), "seoImage", []), "first", [], "method"), "getURL", [], "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 29
        echo "

    <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"/icons/apple-touch-icon.png\">
    <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"/icons/favicon-32x32.png\">
    <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"/icons/favicon-16x16.png\">
    <link rel=\"manifest\" href=\"/icons/site.webmanifest\">


        <style media=\"screen\">

          html {
            box-sizing: border-box;
          }
          *, *:before, *:after {
            box-sizing: inherit;
          }


          @import url(\"https://fast.fonts.net/lt/1.css?apiType=css&c=636f1e27-843b-496c-b2f0-142ed41605a9&fontids=1475956\");
          @font-face{
              font-family:\"Univers LT W01_55 Roman1475956\";
              src:url(\"fonts/1475956/6510377f-160a-4f66-aec0-2b71f05e9736.eot?#iefix\");
              src:url(\"fonts/1475956/6510377f-160a-4f66-aec0-2b71f05e9736.eot?#iefix\") format(\"eot\"),url(\"fonts/1475956/5c8d59c2-9c85-4a22-88e3-bf3fba01dcd8.woff2\") format(\"woff2\"),url(\"fonts/1475956/fc51d03b-0d7d-45df-9415-3c6270c80749.woff\") format(\"woff\"),url(\"fonts/1475956/74bdff6d-c5de-42e8-a4d8-11ea14eddb22.ttf\") format(\"truetype\");
          }


          body, html{
            margin: 0;
            padding: 0;
            background-color: #000;
            color: #fff;
            font-family:\"Univers LT W01_55 Roman1475956\", sans-serif;
            font-size: 30px;
            line-height: 1.3;
          }

          header{
            min-height: 100vw;
            margin-bottom: 50px;
          }

          .circle{
            height: calc(100vw - 50px);
            width: calc(100vw - 50px);
            position: absolute;
            top: 40px;
            left: 50%;
            transform: translate(-50%, 0%);
            background: red;
            border-radius: 50%;
            background-color: rgb(239, 42, 40);
            /* box-shadow: inset 50px -20px 50px rgba(0,0,0,1); */
            background-image: linear-gradient(-130deg, rgba(239, 42, 40, 1) 0%, rgba(217,122, 0, 1) 100%);
            /* animation-name: Gradient;
            animation-duration: 1s;
            animation-iteration-count: infinite;
            animation-timing-function: linear; */
          }

          .circle:before{
            content: '';
            height: calc(100vw - 50px);
            width: calc(100vw - 50px);
            position: absolute;
            top: 0px;
            left: 50%;
            transform: translate(-50%, 0%) scale(1.05);
            border-radius: 50%;
            box-shadow: inset 180px -20px 100px rgba(0,0,0,1);
          }

          @keyframes Gradient {
          \t0% {
          \t\tbackground-position: 0% 50%
          \t}
          \t50% {
          \t\tbackground-position: 100% 50%
          \t}
          \t100% {
          \t\tbackground-position: 0% 50%
          \t}
          }

          /* @media (orientation: portrait) {
            .circle{
              top: 50%;
              transform: translate(-50%, -50%);
              height: calc(100vw - 50px);
              width: calc(100vw - 50px);
            }
          } */

          .header-content-left {
            width: 50%;
            float: right;
            z-index: 2;
            position: relative;
          }

          .header-content-left h1{
            max-width: 500px;
          }

          .content, .form {
            padding-left: 60px;
            padding-right: 200px;
            margin: 0px auto;
            margin-bottom: 90px;
            max-width: 1075px;
          }

          .submissions {
            padding-left: 60px;
            margin: 0px auto;
            margin-bottom: 90px;
            max-width: 1075px;
          }

          footer {
            padding-left: 60px;
            padding-right: 200px;
            margin: 0px auto;
            margin-bottom: 2rem;
            max-width: 1075px;
          }

          .credit-wrap{
            position: relative;
            width: 100%;
          }

          .credit{
            bottom: 43px;
            position: absolute;
            right: -20px;
            transform: rotate(-90deg);
            transform-origin: 0 0;
          }

          h1 {
            font-size: 55px;
            padding: 0 20px;
            font-weight: normal;
            line-height: 1;
          }

          h2, h3{
            margin: 0px;
            font-weight: normal;
            padding: 0 20px;
          }

          p, ul {
            padding:0 20px;
            width: 100%;
            margin: 0px auto;
            margin-bottom: 1.3em;
          }

          ul {
            padding-left: 60px;
          }

          form{
            padding:0 20px;
            width: 100%;
            margin: 0px;
            margin-bottom: 1.3em;
          }

          .form-input{
            margin-bottom: 20px;
            position: relative;
            max-width: 600px;
          }

          .small{
            font-size: 12px;
          }

          .form-input.required:after{
            content: '*';
            font-size: 30px;
            position: absolute;
            right: -25px;
            top: -15px;
            color: #fff;
          }

          .required-checkbox{
            font-size: 20px;
            position: absolute;
            right: -15px;
            top: -5px;
            color: #fff;
          }

          a {
            color: #fff;
            font-size: 12px;
            text-decoration: none;
          }

          a:hover{
            text-decoration: underline;
          }


          input[type=text] {
            -webkit-appearance: none;
            -moz-appearance:none;
            appearance: none;
            border: 3px solid #fff;
            width: 100%;
            max-width:600px;
            background: none;
            font-size:30px;
            padding: 10px 15px;
            outline: none;
            color: #fff;
          }

          input[type=submit] {
            -webkit-appearance: none;
            -moz-appearance:none;
            appearance: none;
            border: 3px solid #fff;
            background: #fff;
            font-size:30px;
            padding: 2px 15px;
            outline: none;
            color: #000;
            cursor: pointer;
          }

          ::placeholder {
            color: rgba(255,255,255,0.5);
          }
          :-ms-input-placeholder {
            color: rgba(255,255,255,0.5);
          }

          ::-ms-input-placeholder {
            color: rgba(255,255,255,0.5);
          }

          li{
            list-style: none;
            margin-bottom: 1.3em;
            max-width: 770px;
          }

          li::before{
            content: '——— ';
            letter-spacing: -14px;
            margin-right: 25px;
          }

          .checkbox-container {
            font-size: 12px;
            display: block;
            position: relative;
            padding-right: 0px;
            margin-bottom: 0px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            width: 250px;
          }

          .checkbox-container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
          }

          .checkmark {
            position: absolute;
            top: 0;
            right: 0;
            height: 15px;
            width: 15px;
            background-color: #000;
            border: 2px solid #fff;
          }

          .checkbox-container:hover input ~ .checkmark {
            background-color: #ccc;
          }

          .checkbox-container input:checked ~ .checkmark {
            background-color: #fff;
          }

          .checkbox-container .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
          }

          .count{
            font-size: 112px;
            padding: 0 20px;
            margin-bottom: 2rem;
          }

          .signatures{
            width: 100%;
            column-count: 4;
            padding: 0 20px;
          }

          .signatures a, .signatures span{
            width: 100%;
            display: block;
            margin: 0px;
            padding-left: 20px;
            text-indent: -20px;
            line-height: 18px;
          }

          footer p{
            font-size: 12px;
          }

          .errormsg{
            display: none;
          }

          .show-error .errormsg{
            display: block;
            color: red;
            margin-top: 5px;
            font-size: 12px;
          }

          @media only screen and (max-width: 800px) {
            .content, .form, ul, .submissions, footer {
              padding-right: 20px;
              padding-left: 20px;
            }
            form{
              padding: 0 10px 0 0;
            }
            .credit{
              position: static;
              transform: none;
              padding-left: 20px;
            }
            .credit-wrap{
              margin-bottom: 50px;
            }
            footer{
              margin-bottom: 10px;
            }

            .circle:before{
              box-shadow: inset 80px -20px 80px rgba(0,0,0,1);
            }

            .signatures{
              column-count: 3;
            }

          }

          @media only screen and (max-width: 600px) {
            h1 {
              font-size: 30px;
            }
            p, li, input[type=text], input[type=submit]{
              font-size: 18px;
            }

            p, li{
              padding: 0;
            }

            .header-content-left {
              width: auto;
            }

            .form-input.required:after{
              content: '*';
              font-size: 18px;
              right: -15px;
              top: -5px;
            }

            .signatures{
              column-count: 2;
            }

          }

          @media only screen and (max-width: 420px) {
            .signatures{
              column-count: 1;
            }
          }

        </style>
";
        // line 441
        call_user_func_array($this->env->getFunction('head')->getCallable(), []);
        echo "</head>
<body>";
        // line 442
        call_user_func_array($this->env->getFunction('beginBody')->getCallable(), []);
        echo "

<header>
  <div class=\"header-content-left\">
    <h1>";
        // line 446
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "siteTitle", []), "html", null, true);
        echo "</h1>
  </div>
  <div class=\"circle\">

  </div>
</header>
<div class=\"content\">
  ";
        // line 453
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "siteDescription", []), "html", null, true);
        echo "
</div>
<div class=\"form\">
  ";
        // line 456
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "formDescription", []), "html", null, true);
        echo "
<form method=\"post\" action=\"\" accept-charset=\"UTF-8\" id=\"sign-form\"  >

    ";
        // line 459
        echo twig_escape_filter($this->env, $this->extensions['craft\web\twig\Extension']->csrfInputFunction(), "html", null, true);
        echo "

    <input type=\"hidden\" name=\"action\" value=\"guest-entries/save\">
    <input type=\"hidden\" name=\"sectionId\" value=\"1\">

    ";
        // line 464
        echo twig_escape_filter($this->env, $this->extensions['craft\web\twig\Extension']->redirectInputFunction("success"), "html", null, true);
        echo "

    <div class=\"form-input required\">
      <input id=\"nameOfPractice\" type=\"text\" name=\"fields[nameOfPractice]\" placeholder=\"";
        // line 467
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "nameOfPracticeText", []), "html", null, true);
        echo "\" value=\"\" >
      <span class=\"errormsg\">";
        // line 468
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "nameOfPracticeError", []), "html", null, true);
        echo "</span>
    </div>

    <div class=\"form-input required\">
      <input id=\"website\" type=\"text\" name=\"fields[website]\" placeholder=\"";
        // line 472
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "websiteText", []), "html", null, true);
        echo "\" value=\"\" >
      <span class=\"errormsg\">";
        // line 473
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "websiteError", []), "html", null, true);
        echo "</span>
    </div>

    <div class=\"form-input required\">
      <input id=\"contactEmail\" type=\"text\" name=\"fields[contactEmail]\" placeholder=\"Email\" value=\"\"  >
      <span class=\"errormsg\">";
        // line 478
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "emailError", []), "html", null, true);
        echo "</span>
    </div>

    <div class=\"form-input\">
      <label for= class=\"checkbox-container\">";
        // line 482
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "consentText", []), "html", null, true);
        echo "
        <span class=\"required-checkbox\">*</span>
        <input id=\"dataConsent\" name=\"\" type=\"checkbox\" >
        <span class=\"checkmark\"></span>
      </label>
      <span class=\"errormsg\">";
        // line 487
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "consentError", []), "html", null, true);
        echo "</span>
    </div>
    <div class=\"form-input\">
      <label class=\"checkbox-container\">";
        // line 490
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "updateText", []), "html", null, true);
        echo "
        <input type=\"checkbox\" >
        <span class=\"checkmark\"></span>
      </label>
    </div>
    <div class=\"form-input small\">
      *";
        // line 496
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "requireFieldsText", []), "html", null, true);
        echo "
    </div>

    <input type=\"submit\" value=\"SUBMIT\">
</form>
</div>
<div class=\"submissions\">
  ";
        // line 503
        $context["entries"] = craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["craft"] ?? null), "entries", []), "section", [0 => "signatures"], "method"), "order", [0 => "title asc"], "method"), "limit", [0 => null], "method");
        // line 504
        echo "  ";
        $context["entryCount"] = twig_length_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, craft\helpers\Template::attribute($this->env, $this->source, ($context["craft"] ?? null), "entries", []), "section", [0 => "signatures"], "method"), "anyStatus", [], "method"), "order", [0 => "title asc"], "method"), "limit", [0 => null], "method"));
        // line 505
        echo "
  <h3>";
        // line 506
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "signaturesText", []), "html", null, true);
        echo "</h3>
  <div class=\"count\">";
        // line 507
        echo twig_escape_filter($this->env, ($context["entryCount"] ?? null), "html", null, true);
        echo "</div>

  <div class=\"signatures\">
    ";
        // line 510
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["entries"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["signiture"]) {
            // line 511
            echo "      ";
            $context["url"] = (((twig_slice($this->env, craft\helpers\Template::attribute($this->env, $this->source, $context["signiture"], "website", []), 0, 4) == "http")) ? (craft\helpers\Template::attribute($this->env, $this->source, $context["signiture"], "website", [])) : (("http://" . craft\helpers\Template::attribute($this->env, $this->source, $context["signiture"], "website", []))));
            // line 512
            echo "      ";
            if (craft\helpers\Template::attribute($this->env, $this->source, $context["signiture"], "website", [])) {
                echo "<a target=\"_blank\" href=\"";
                echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
                echo "\">";
            } else {
                echo "<span>";
            }
            echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, $context["signiture"], "nameOfPractice", []), "html", null, true);
            if (craft\helpers\Template::attribute($this->env, $this->source, $context["signiture"], "website", [])) {
                echo "</a>";
            } else {
                echo "</span>";
            }
            // line 513
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['signiture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 514
        echo "  </div>
</div>

<footer>
  ";
        // line 518
        echo twig_escape_filter($this->env, craft\helpers\Template::attribute($this->env, $this->source, ($context["pageContent"] ?? null), "footerText", []), "html", null, true);
        echo "
</footer>
<div class=\"credit-wrap\">
  <a href=\"https://lovers.co/\" class=\"credit\">Site by Lovers</a>
</div>



<script type=\"text/javascript\">
  var nameOfPractice = document.getElementById('nameOfPractice');
  var website = document.getElementById('website');
  var contactEmail = document.getElementById('contactEmail');
  var dataConsent = document.getElementById('dataConsent');

  nameOfPractice.addEventListener('change', validateCheck);
  website.addEventListener('change', validateCheck);
  contactEmail.addEventListener('change', validateCheck);
  dataConsent.addEventListener('change', validateCheck);

  function validateCheck(){
    var elm = this;
    if(elm.value.length > 0){
      removeClass(elm.parentNode, 'show-error');
    }
    if(elm.type === \"checkbox\") {
      if(elm.checked){
        removeClass(elm.parentNode.parentNode, 'show-error');
      }
    }
  }


  document.getElementById('sign-form').addEventListener('submit', function(evt){

    var validation = true;

    if(website.value.length < 1){
      validation = false;
      addClass(website.parentNode, 'show-error');
    }

    if(nameOfPractice.value.length < 1 ){
      validation = false;
      addClass(nameOfPractice.parentNode, 'show-error');
    }

    if(contactEmail.value.length < 1){
      validation = false;
      addClass(contactEmail.parentNode, 'show-error');
    }

    if(!dataConsent.checked){
      validation = false;
      addClass(dataConsent.parentNode.parentNode, 'show-error');
    }

    if(!validation){
      evt.preventDefault();
    }

  })

  function hasClass(ele,cls) {
    return !!ele.className.match(new RegExp('(\\\\s|^)'+cls+'(\\\\s|\$)'));
  }

  function addClass(ele,cls) {
    if (!hasClass(ele,cls)) ele.className += \" \"+cls;
  }

  function removeClass(ele,cls) {
    if (hasClass(ele,cls)) {
      var reg = new RegExp('(\\\\s|^)'+cls+'(\\\\s|\$)');
      ele.className=ele.className.replace(reg,' ');
    }
  }

</script>

";
        // line 597
        call_user_func_array($this->env->getFunction('endBody')->getCallable(), []);
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "index";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  764 => 597,  682 => 518,  676 => 514,  670 => 513,  655 => 512,  652 => 511,  648 => 510,  642 => 507,  638 => 506,  635 => 505,  632 => 504,  630 => 503,  620 => 496,  611 => 490,  605 => 487,  597 => 482,  590 => 478,  582 => 473,  578 => 472,  571 => 468,  567 => 467,  561 => 464,  553 => 459,  547 => 456,  541 => 453,  531 => 446,  524 => 442,  520 => 441,  106 => 29,  100 => 27,  98 => 26,  93 => 24,  89 => 23,  85 => 22,  79 => 19,  74 => 17,  70 => 16,  66 => 15,  63 => 14,  57 => 12,  55 => 11,  48 => 7,  44 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "index", "/Users/simonneveu/Workspace/architects-climate/templates/index.html");
    }
}
